<h1>Parent POM</h1>  
<h2>Description</h2>  
  
Parent POM to aggregate all the common set up for my projects in a convenient place.  
  
<h2>Versioning</h2> 
 
In order to follow Semantic Versioning I have included the following solution:  

The parent POM will automatically update its own version with every commit/merge to master branch.  
To that end I have used versioning Python script found here: https://gitlab.com/threedotslabs/ci-scripts/blob/master/common/gen-semver.  
This Python script can be found here in my repository: https://gitlab.com/Pandaniel/scripts  
The script will generate the Git Tag resembling the SemVer and output it to a file.  
  
<h2>Deployment</h2>  

I have used https://mymavenrepo.com/ to have a free place to deploy my generated pom to.  
I have created a simple script that runs Maven Version Plugin with version generated from the above Python Script as an argument.  
The script later commits the changed version as a commit to master.   
Since the script auto-versions the POM, it is easy to grab it from the repository.   
The ready parent POM can be downloaded by including the correct repository in the child POM as such:  

    <repositories>
        <repository>
            <id>myMavenRepoRead</id>
            <url>${myMavenRepoReadUrl}</url>
        </repository>
    </repositories>

And then can be attached to the project just by adding it as follows:

    <parent>
        <groupId>com.daniel</groupId>
        <artifactId>parent_pom</artifactId>
        <version>1.0.3</version>
    </parent>
